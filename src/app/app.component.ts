import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'users';
  currentUrl = 'login';
  constructor(private router : Router) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    )
      .subscribe(event => {
          console.log(event);
          this.currentUrl = event['url'];
      });
   
  }

  navPage(route){
    this.router.navigate(['/'+route+'']);
  }

}
