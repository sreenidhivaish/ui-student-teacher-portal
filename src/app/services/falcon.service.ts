import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FalconService {
  constructor(private http:HttpClient) {}

  userName : any;
  semester : any;
  //private userUrl = 'http://localhost:8080/user-portal/user';
	private userUrl = 'http://localhost:8080/';

  public getUserByName(userName) {
    this.userName = userName;
    return this.http.get(this.userUrl + "getUserByName" +'/'+ this.userName);
  }

  public getBySemester(sem) {
    this.semester = sem;
    return this.http.get(this.userUrl + "getBySemester" +'/'+ this.semester);
  }

  public getStudentByName(name) {
    return this.http.get(this.userUrl + "getStudentByName" +'/'+ name);
  }

}
