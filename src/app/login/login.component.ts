import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { FalconService } from '../services/falcon.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userName: any;
  password: any;
  allowStudent = false;
  allowTeacher = false;
  constructor(private router: Router, private dataService: DataService, private falconService: FalconService) { }

  ngOnInit(): void {

  }

  login() {

    this.falconService.getUserByName(this.userName).subscribe(data => {
      console.log(data);
      console.log(data['userName']);
      if (this.userName === data['userName'] && this.password === data['password'] && data['role'] === 'S') {
        
        this.allowStudent = true;
        this.allowStudentM();
      }
      if (this.userName == data['userName'] && this.password == data['password'] && data['role'] == 'T') {
        this.allowTeacher = true;
        this.allowTeacherM();
      }
    });
console.log(this.allowStudent);
// if(this.allowStudent) {
//   this.router.navigate(['/student']);
// }
// if(this.allowTeacher) {
//   this.router.navigate(['/teacher']);
}
allowStudentM() {
  this.dataService.userName = this.userName;
  this.router.navigate(['/student']);
}

allowTeacherM() {
  this.dataService.userName = this.userName;
  this.router.navigate(['/teacher']);
}
  }
  
