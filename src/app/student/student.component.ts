import { Component, OnInit } from '@angular/core';
import { FalconService } from '../services/falcon.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
  student : any;
  semester : number;
  dummy : any[];
  constructor(private falconService: FalconService,  private data : DataService) { }

  ngOnInit(): void {
    
    this.falconService.getStudentByName(this.data.userName).subscribe(data => {
      console.log(data);
      this.student = data;
    });
  }

  getBySemester(semester) {
    this.falconService.getBySemester(this.semester).subscribe(data => {
      console.log(data);
      this.student.push(data);
      this.dummy.push(data);
      console.log(this.dummy)
    });
    this.semester = semester;
   

}

}
